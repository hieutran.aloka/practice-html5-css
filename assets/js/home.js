$('document').ready(function(){
    simpleSlick();
});

function simpleSlick(){
    var classImage = '.hero-image-';
    var length = 4;
    var count = 0;
    setInterval(function(){
        if (count != 0){
            if (count != length){
                $(linkStringAndNumber(classImage, count)).css('display', 'block');
                $(linkStringAndNumber(classImage, count-1)).css('display','none');
            } else{
                $(linkStringAndNumber(classImage, count-1)).css('display','none');
                count = 0;
                $(linkStringAndNumber(classImage, count)).css('display', 'block');
            }
        } else{
            $(linkStringAndNumber(classImage, count)).css('display', 'block');
            $(linkStringAndNumber(classImage, length-1)).css('display','none');
        }
        count = count+1;
    },1500);
}
function linkStringAndNumber(defaultString, number){
    return defaultString+number;
}